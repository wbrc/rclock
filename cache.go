package rclock

import (
	"log"
	"math"
	"sync"

	"github.com/OneOfOne/xxhash"
)

const (
	evBufSize uint64 = 1024
)

type Cache struct {
	iSeg    *insertSegment
	pqSeg   *pqSegment
	maxCost uint64

	lock *sync.RWMutex
}

func New(maxCost uint64) *Cache {
	log.Print("create")
	return &Cache{
		iSeg:    newInsertSegment(maxCost/2, evBufSize),
		pqSeg:   newPQSegment(maxCost/2, evBufSize),
		maxCost: maxCost,
		lock:    &sync.RWMutex{},
	}
}

func (c *Cache) Delete(key string) bool {
	intKey := xxhash.ChecksumString64(key)
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.iSeg.del(intKey) {
		return true

	}
	return c.pqSeg.del(intKey)
}

func (c *Cache) Get(key string) ([]byte, bool) {
	intKey := xxhash.ChecksumString64(key)
	c.lock.RLock()
	defer c.lock.RUnlock()

	v, ok := c.iSeg.get(intKey)
	if ok {
		return v, true
	}

	v, ok = c.pqSeg.get(intKey)
	if ok {
		return v, true
	}
	return nil, false
}

func (c *Cache) Insert(key string, value []byte, cost uint64) {
	intKey := xxhash.ChecksumString64(key)

	elems := []*cacheElement{newCacheElement(intKey, value, cost, iqEvictable)}

	c.lock.Lock()
	defer c.lock.Unlock()

	if !c.iSeg.del(intKey) {
		c.pqSeg.del(intKey)
	}

	c.iSeg.insert(elems, cost)

	var cumulated uint64
	for _, v := range c.iSeg.promotionBuf {
		cumulated += v.cost
	}

	if cumulated > 0 {
		c.pqSeg.insert(c.iSeg.promotionBuf, cumulated)
	}

	tot := c.iSeg.eCnt + c.pqSeg.eCnt
	isr := float64(c.iSeg.eCnt) / float64(tot)
	isr = math.Max(0.1, isr)
	isr = math.Min(0.9, isr)

	c.iSeg.maxCost = uint64(float64(c.maxCost) * isr)
	c.pqSeg.maxCost = c.maxCost - c.iSeg.maxCost
}
