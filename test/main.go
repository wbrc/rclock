package main

import (
	"fmt"
	"math/rand"

	"gitlab.com/wbrc/rclock"
)

func main() {
	c := rclock.New(1024 * 1024)

	h, m := 0, 0

	for i := 0; i < 1000000; i++ {
		if rand.Float64() < 0.3 {
			key := fmt.Sprint(rand.Intn(10000))
			val := make([]byte, rand.Intn(100)+50)
			rand.Read(val)
			c.Insert(key, val, uint64(cap(val)))
		} else {
			key := fmt.Sprint(rand.Intn(10000))
			_, ok := c.Get(key)
			if ok {
				h++
			} else {
				m++
			}
		}
	}

	hr := float64(h) / float64(h+m)
	fmt.Printf("Hits:   %d\n", h)
	fmt.Printf("Misses: %d\n", m)
	fmt.Printf("Ratio:  %f\n", hr)
}
