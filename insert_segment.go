package rclock

import "sync/atomic"

type insertSegment struct {
	values        map[uint64]*cacheElement
	cost, maxCost uint64
	evictionBuf   []uint64

	evicted  map[uint64]*uint32
	eSize    uint64
	maxESize uint64
	eCnt     uint64

	promotionBuf []*cacheElement
}

func newInsertSegment(maxCost, maxESize uint64) *insertSegment {
	return &insertSegment{
		values:   make(map[uint64]*cacheElement),
		maxCost:  maxCost,
		evicted:  make(map[uint64]*uint32),
		maxESize: maxESize,
	}
}

func (seg *insertSegment) del(key uint64) bool {
	elem, ok := seg.values[key]
	if ok {
		delete(seg.values, key)
		seg.cost -= elem.cost
		return true
	}
	return false
}

func (seg *insertSegment) get(key uint64) ([]byte, bool) {
	elem, ok := seg.values[key]
	if ok {
		if !atomic.CompareAndSwapUint32(&elem.flag, 0, 1) {
			atomic.CompareAndSwapUint32(&elem.flag, 1, 2)
		}
		return elem.value, true
	}

	if _, ok := seg.evicted[key]; ok {
		atomic.StoreUint32(seg.evicted[key], 1)
		atomic.AddUint64(&seg.eCnt, 1)
	}

	return nil, false
}

func (seg *insertSegment) insert(elems []*cacheElement, totalCost uint64) {

	seg.evictionBuf = seg.evictionBuf[:0]
	seg.promotionBuf = seg.promotionBuf[:0]
outer:
	for totalCost+seg.cost > seg.maxCost {
		for k, v := range seg.values {
			if v.flag == iqEvictable {
				delete(seg.values, k)
				seg.cost -= v.cost
				seg.evictionBuf = append(seg.evictionBuf, k)
			} else if v.flag == iqRef {
				v.flag = iqEvictable
			} else {
				delete(seg.values, k)
				seg.cost -= v.cost
				seg.promotionBuf = append(seg.promotionBuf, v)
			}

			if totalCost+seg.cost <= seg.maxCost {
				break outer
			}
		}
	}

	seg.cost += totalCost
	for _, e := range elems {
		seg.values[e.key] = e
	}

evloop:
	for uint64(len(seg.evictionBuf))+seg.eSize > seg.maxESize {
		for k, v := range seg.evicted {
			if *v == 0 {
				delete(seg.evicted, k)
				seg.eSize--
			} else {
				*seg.evicted[k] = 0
			}

			if uint64(len(seg.evictionBuf))+seg.eSize <= seg.maxESize {
				break evloop
			}
		}
	}

	for _, k := range seg.evictionBuf {
		d := new(uint32)
		*d = 1
		seg.evicted[k] = d
	}
}
