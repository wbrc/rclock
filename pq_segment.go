package rclock

import "sync/atomic"

type pqSegment struct {
	values        map[uint64]*cacheElement
	cost, maxCost uint64
	evictionBuf   []uint64

	evicted  map[uint64]*uint32
	eSize    uint64
	maxESize uint64
	eCnt     uint64
}

func newPQSegment(maxCost, maxESize uint64) *pqSegment {
	return &pqSegment{
		values:   make(map[uint64]*cacheElement),
		maxCost:  maxCost,
		evicted:  make(map[uint64]*uint32),
		maxESize: maxESize,
	}
}

func (seg *pqSegment) del(key uint64) bool {
	elem, ok := seg.values[key]
	if ok {
		delete(seg.values, key)
		seg.cost -= elem.cost
		return true
	}
	return false
}

func (seg *pqSegment) get(key uint64) ([]byte, bool) {
	elem, ok := seg.values[key]
	if ok {
		atomic.CompareAndSwapUint32(&elem.flag, 2, 3)
		return elem.value, true
	}

	if _, ok := seg.evicted[key]; ok {
		atomic.StoreUint32(seg.evicted[key], 1)
		atomic.AddUint64(&seg.eCnt, 1)
	}

	return nil, false
}

func (seg *pqSegment) insert(elems []*cacheElement, totalCost uint64) {

	seg.evictionBuf = seg.evictionBuf[:0]
outer:
	for totalCost+seg.cost > seg.maxCost {
		for k, v := range seg.values {
			if v.flag == pqEvictable {
				delete(seg.values, k)
				seg.cost -= v.cost
				seg.evictionBuf = append(seg.evictionBuf, k)
			} else {
				v.flag = pqEvictable
			}

			if totalCost+seg.cost <= seg.maxCost {
				break outer
			}
		}
	}

	seg.cost += totalCost
	for _, e := range elems {
		seg.values[e.key] = e
	}

evloop:
	for uint64(len(seg.evictionBuf))+seg.eSize > seg.maxESize {
		for k, v := range seg.evicted {
			if *v == 0 {
				delete(seg.evicted, k)
				seg.eSize--
			} else {
				*seg.evicted[k] = 0
			}

			if uint64(len(seg.evictionBuf))+seg.eSize <= seg.maxESize {
				break evloop
			}
		}
	}

	for _, k := range seg.evictionBuf {
		d := new(uint32)
		*d = 1
		seg.evicted[k] = d
	}
}
