package rclock

const (
	iqEvictable = 0
	iqRef       = 1
	iqPro       = 2

	pqEvictable = 2
	pqRef       = 3
)

type cacheElement struct {
	key   uint64
	value []byte
	cost  uint64
	flag  uint32
}

func newCacheElement(key uint64, value []byte, cost uint64,
	flag uint32) *cacheElement {
	return &cacheElement{
		key:   key,
		value: value,
		cost:  cost,
		flag:  uint32(flag),
	}
}
